#!/bin/bash

echo 'start Tomcat-Container ...'
docker rm -f tomcat 2> /dev/null
docker run --name tomcat -P -d tomcat
