package de.akquinet.vacationplanner.rest;

import static org.junit.Assert.assertTrue;

import java.util.GregorianCalendar;

import javax.ws.rs.core.MediaType;

import org.junit.Test;

import de.akquinet.vacationplanner.dtos.Employee;
import de.akquinet.vacationplanner.dtos.Vacation;

/**
 * Tests the vacation REST service.
 */
public class VacationResourceTest extends AbstractRestTest {

	/**
	 * Tests the creation and deletion of a vacation.
	 */
	@Test
	public void testAddAndDeleteEmployeeWithVacation() {
		Employee employee = new Employee();
		employee.setGivenName("Franz");
		employee.setGivenName("Meier");
		employee.setId("5");

		getService().path("employees").put(employee);

		Vacation vacation = new Vacation();
		vacation.setStartDate(new GregorianCalendar(2014, 5, 1).getTime());
		vacation.setEndDate(new GregorianCalendar(2014, 5, 11).getTime());
		vacation.setId("104");
		getService().path("employees").path("5").put(vacation);

		String response = getService().path("employees").path("5")
				.accept(MediaType.TEXT_XML).get(String.class);
		assertTrue("response doesn't contain '<vacations>': " + response,
				response.contains("<vacations>"));

		getService().path("employees").path("5").path("vacations").path("104")
				.delete();

		response = getService().path("employees").path("5")
				.accept(MediaType.TEXT_XML).get(String.class);
		assertTrue("response does contain '<vacations>': " + response,
				!response.contains("<vacations>"));

		getService().path("employees").path("5").delete();
	}
}
