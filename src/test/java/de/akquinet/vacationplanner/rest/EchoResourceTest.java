package de.akquinet.vacationplanner.rest;

import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.MediaType;

import org.junit.Test;

/**
 * Tests the echo REST service.
 */
public class EchoResourceTest extends AbstractRestTest {

	/**
	 * Tests the echo service and asserts that a Hello students string is
	 * returned.
	 */
	@Test
	public void testGet() {
		String response = getService().path("echo").accept(MediaType.TEXT_HTML)
				.get(String.class);

		assertTrue("response is empty", !response.isEmpty());
		assertTrue("response doesn't contain 'Hello students': " + response,
				response.contains("Hello students"));
	}

}
