package de.akquinet.vacationplaner.servicemock;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.akquinet.vacationplanner.dtos.Employee;
import de.akquinet.vacationplanner.dtos.Vacation;

/**
 * A mock implementation of the internal service, just for demonstration. Data
 * is stored temporary without any database access.
 */
public class EmployeeServiceMock {
	private static Map<String, Employee> employees = new HashMap<String, Employee>();
	private static int lastUniqueEmployeeId = 0;
	private static int lastUniqueVacationId = 0;

	/**
	 * Creates some mock data (employees and vacations).
	 */
	static {
		Employee employee = new Employee();
		employee.setGivenName("Max");
		employee.setLastName("Mustermann");

		Vacation vacation = new Vacation();
		vacation.setStartDate(new GregorianCalendar(2014, 1, 1).getTime());
		vacation.setEndDate(new GregorianCalendar(2014, 1, 10).getTime());
		addOrUpdateVacation(vacation, employee);

		Vacation vacation2 = new Vacation();
		vacation2.setStartDate(new GregorianCalendar(2014, 5, 4).getTime());
		vacation2.setEndDate(new GregorianCalendar(2014, 5, 14).getTime());
		addOrUpdateVacation(vacation2, employee);

		addOrUpdateEmployee(employee);

		Employee employee2 = new Employee();
		employee2.setGivenName("Petra");
		employee2.setLastName("Musterfrau");
		addOrUpdateEmployee(employee2);
	}

	/**
	 * Returns the employee by his unique identifier.
	 * 
	 * @param id
	 *            employee ID
	 * @return employee
	 */
	public static Employee getEmployee(String id) {
		return employees.get(id);
	}

	/**
	 * Returns a list of all employees without any vacation references.
	 * 
	 * @return list of employees
	 */
	public static List<Employee> getEmployees() {
		List<Employee> employees = new ArrayList<Employee>();

		for (Entry<String, Employee> entry : EmployeeServiceMock.employees
				.entrySet()) {
			Employee employee = new Employee();
			employee.setId(entry.getValue().getId());
			employee.setGivenName(entry.getValue().getGivenName());
			employee.setLastName(entry.getValue().getLastName());
			employees.add(employee);
		}

		return employees;
	}

	/**
	 * Adds or updates an employee.
	 * 
	 * @param employee
	 *            employee
	 */
	public static void addOrUpdateEmployee(Employee employee) {
		if (employee.getId() == null) {
			lastUniqueEmployeeId = lastUniqueEmployeeId + 1;
			employee.setId("" + lastUniqueEmployeeId);
		}

		employees.put(employee.getId(), employee);
	}

	/**
	 * Adds or updates a vacation to the given employee.
	 * 
	 * @param vacation
	 *            vacation
	 * @param employee
	 *            employee
	 */
	public static void addOrUpdateVacation(Vacation vacation, Employee employee) {
		if (vacation.getId() == null) {
			lastUniqueVacationId = lastUniqueVacationId + 1;
			vacation.setId("" + lastUniqueVacationId);
		}

		employee.addVacation(vacation);
	}

	/**
	 * Deletes an employee.
	 * 
	 * @param id
	 *            employee ID
	 */
	public static void deleteEmployee(String id) {
		employees.remove(id);
	}

	/**
	 * Deletes a vacation.
	 * 
	 * @param id
	 *            employee ID
	 */
	public static void deleteVacation(String vacationId, Employee employee) {
		for (Vacation vacation : employee.getVacations()) {
			if (vacation.getId().equals(vacationId)) {
				employee.removeVacation(vacation);
				break;
			}
		}
	}
}
