package de.akquinet.vacationplanner.dtos;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * POJO contains the vacation data.
 */
@XmlRootElement
public class Vacation {
	private String id;
	private Date startDate;
	private Date endDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
