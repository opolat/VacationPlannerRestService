package de.akquinet.vacationplanner.dtos;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * POJO contains the employee data.
 */
@XmlRootElement
public class Employee {
	private String id;
	private String givenName;
	private String lastName;
	private List<Vacation> vacations = new ArrayList<Vacation>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Vacation> getVacations() {
		return vacations;
	}

	public void setVacations(List<Vacation> vacations) {
		this.vacations = vacations;
	}

	public void addVacation(Vacation vacation) {
		vacations.add(vacation);
	}

	public void removeVacation(Vacation vacation) {
		vacations.remove(vacation);
	}
}
