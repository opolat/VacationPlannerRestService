package de.akquinet.vacationplanner.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Rest service for a simple echo server (e.g. for web service available tests).
 */
@Path("/echo")
public class EchoResource {

	/**
	 * Returns a simple echo in HTML format.
	 * <p />
	 * Invoke this URL to see the GET result:
	 * http://localhost:8080/VacationPlannerRestService/echo
	 */
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String sayHello() {
		return "<html><body><h1>Hello</h1><p>Hello students here is REST!</p></body></html> ";
	}
}
