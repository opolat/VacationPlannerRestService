package de.akquinet.vacationplanner.rest;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import de.akquinet.vacationplaner.servicemock.EmployeeServiceMock;
import de.akquinet.vacationplanner.dtos.Employee;
import de.akquinet.vacationplanner.dtos.Vacation;

/**
 * Rest service for CRUD operation regarding a specific vacation.
 */
public class VacationResource {
	private String employeeId;
	private String vacationId;

	/**
	 * Initializes the resource with the unique identifiers.
	 * 
	 * @param employeeId
	 *            employee ID
	 * @param vacationId
	 *            vacation ID
	 */
	public VacationResource(String employeeId, String vacationId) {
		this.employeeId = employeeId;
		this.vacationId = vacationId;
	}
	
	/**
	 * Returns the data of an vacation.
	 * <p />
	 * Invoke this URL to see the GET result (e.g. employee with the ID 1):
	 * http://localhost:8080/VacationPlannerRestService/employees/1/vacations/1
	 * 
	 * @return employee
	 */
	@GET
	@Produces({ MediaType.TEXT_XML })
	public Vacation getVacation() {
		List<Vacation> vacations = EmployeeServiceMock.getEmployee(employeeId).getVacations();
		for (Vacation vacation : vacations) {
			if(vacation.getId().equals(vacationId)) {
			  return vacation; 	
			}
		}
		return null;
	}
	
	/**
	 * Deletes the vacation.
	 * <p />
	 * Invoke this URL with DELETE (e.g. the employee with the ID 1 and vacation
	 * ID 104):
	 * http://localhost:8080/VacationPlannerRestService/employees/1/vacations
	 * /104
	 */
	@DELETE
	public void deleteVacation() {
		Employee employee = EmployeeServiceMock.getEmployee(employeeId);

		if (employee != null) {
			EmployeeServiceMock.deleteVacation(vacationId, employee);
		}
	}
}
