package de.akquinet.vacationplanner.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import de.akquinet.vacationplaner.servicemock.EmployeeServiceMock;
import de.akquinet.vacationplanner.dtos.Vacation;

/**
 * Rest service for CRUD operation regarding employee vacations.
 */
public class VacationsResource {
	private String employeeId;

	/**
	 * Initializes the resource with the unique identifier of the employee.
	 * 
	 * @param employeeId
	 *            employee ID
	 */
	public VacationsResource(String employeeId) {
		this.employeeId = employeeId;
	}
	
	/**
	 * Returns the data of all vacations for an employees.
	 * <p />
	 * Invoke this URL to see the GET result (e.g. employee with the ID 1):
	 * http://localhost:8080/VacationPlannerRestService/employees/1/vacations
	 * 
	 * @return employee
	 */
	@GET
	@Produces({ MediaType.TEXT_XML })
	public List<Vacation> getVacation() {
		return EmployeeServiceMock.getEmployee(employeeId).getVacations();
	}	

	/**
	 * Creates the sub URL path with the given vacation ID.
	 */
	@Path("{id}")
	public VacationResource getVacation(@PathParam("id") String id) {
		return new VacationResource(employeeId, id);
	}
}
