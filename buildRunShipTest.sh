#!/bin/bash

gradle clean
gradle war
sh startTomcatContainer.sh
sleep 5
sh deployToContainer.sh
sleep 10
port=$(docker inspect tomcat --format '{{ (index (index .NetworkSettings.Ports "8080/tcp") 0).HostPort }}')
curl http://localhost:$port/VacationPlannerRestService/echo
