# Dokumentation
Branch enthält ein Jenkinsfile für eine multi-branch-pipeline demo
Die REST Webservices werden über eine Jenkins pipeline contanerisiert und jeweils pro
branch in einen eigenen Tomcat-Container deployt.

# Starten eines Jenkins
> docker network create eng
> docker run -it --name eng --name jenkins -p 8080:8080 -v $(pwd)/jenkins_home:jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -d opolat/jenkins:latest

* Multi-Branch-Pipeline in Jenkins anlegen
* Behavior zhinzufügen -> Filter by name (with wildcards) und include auf feature* setzen 
* zum demonstrieren unterschiedliche branches anlegen feature*
* um die Links in der Build Historie anzuzeigen unter "Manage Jenkins/Configure Global Security" MArkup Formatter auf Safe HTML setzen




